# GoCoCMS

This is a simple blog engine built on the ASP.NET Core 2.1 

# Demo
Live on Windows Azure: 
- Link: http://gococms.site
- Admin account: admin@gococms.com/P@ssword

# Repository

- Clone the repository using the command "git clone https://gitlab.com/nlkhang.it/gococms.git" and checkout the develop branch.
- It's also available on Github: https://github.com/nlkhang/GoCoCMS 

# Features

## For blog owner (Admin):

- User Login/Logout
- Manage categories
- Manage post
- Manage comments

## For internet users:
- User login/logout, register user
- External login with Google
- Show lastest posts, categories menu
- View posts by category
- View post details
- View post on home page
- Add comments
- Search post

# Technologies used

- ASP.NET Core 2.1
- Entity Framework Core
- ASP.NET Core Identity
- Dependency Injection
- Custom View Component
- Custom Tag Helper
- AutoMapper
- Memory Cache
- Redis Cache
- External login (Google)
- Url Rewrite (slug)
- Unit Testing (XUnit, Moq)

# Visual Studio 2017

- Download:  https://www.visualstudio.com/downloads/
- Open GoCoCMS.sln and wait for VS restore all Nuget Packages
- In Package Manager Console -> Target GoCoCMS.Data -> Let's input a command: update-database
- Set startup project: GoCoCMS.Web
- Run it

# Features in the coming time

- Support multiple themes
- Plugins (install/uninstall)
- Slideshow image on home page
- Support multiple language
- Search Engine Optimization (SEO)
- Manage users
- Manage widget
- Count visits