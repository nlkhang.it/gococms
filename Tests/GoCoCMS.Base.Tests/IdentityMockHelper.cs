﻿using Microsoft.AspNetCore.Identity;
using Moq;
using System.Security.Claims;

namespace GoCoCMS.Base.Tests
{
    public static class IdentityMockHelper
    {
        public static Mock<UserManager<TUser>> MockUserManager<TUser>() where TUser : class 
        {
            var store = new Mock<IUserStore<TUser>>();
            var userManagerMock = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            userManagerMock.Object.UserValidators.Add(new UserValidator<TUser>());
            userManagerMock.Object.PasswordValidators.Add(new PasswordValidator<TUser>());
            userManagerMock.Setup(u => u.GetUserId(It.IsAny<ClaimsPrincipal>())).Returns("1");
            return userManagerMock;
        }
    }
}
