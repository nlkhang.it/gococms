﻿using AutoMapper;
using GoCoCMS.Core.Mapper;
using System;
using System.Linq;

namespace GoCoCMS.Base.Tests
{
    public class ControllerTestBase : IDisposable
    {
        public ControllerTestBase()
        {
            InitAutoMapper();
        }

        private static void InitAutoMapper()
        {
            // get all instances of IMapperProfile
            var mapperProfileType = typeof(IMapperProfile);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(t => t.GetTypes())
                .Where(t => mapperProfileType.IsAssignableFrom(t) && t.IsClass && !t.IsAbstract);

            // mapping config
            var mappingConfig = new MapperConfiguration(mc =>
            {
                foreach (var type in types)
                    mc.AddProfile(type);
            });

            // init auto mapper
            AutoMapperConfiguration.Init(mappingConfig);
        }

        public void Dispose()
        {
            
        }
    }
}
