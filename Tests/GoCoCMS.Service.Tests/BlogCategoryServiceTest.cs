﻿using GoCoCMS.Core.Caching;
using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GoCoCMS.Service.Tests
{
    public class BlogCategoryServiceTest
    {
        [Fact]
        public void GetAllCategories_WithCategoryName_ShouldReturnListData()
        {
            // arrange
            var categoryRepositoryMock = new Mock<IRepository<BlogCategory>>();
            categoryRepositoryMock.Setup(c => c.Table)
                .Returns(new List<BlogCategory>()
                {
                    new BlogCategory()
                    {
                        Id = 1,
                        Name = "ASP.NET MVC",
                        DisplayOrder = 4
                    },
                    new BlogCategory()
                    {
                        Id = 2,
                        Name = "ASP.NET CORE 1.0",
                        DisplayOrder = 2
                    },
                    new BlogCategory()
                    {
                        Id = 3,
                        Name = "ASP.NET CORE 1.1",
                        Deleted = true,
                        DisplayOrder = 3
                    },
                    new BlogCategory()
                    {
                        Id = 4,
                        Name = "ASP.NET CORE 2.1",
                        DisplayOrder = 1
                    }
                }.AsQueryable());
            var cacheManagerMock = new Mock<IStaticCacheManager>();
            var blogCategoryService = new BlogCategoryService(categoryRepositoryMock.Object, cacheManagerMock.Object);

            // action
            var categories = blogCategoryService.GetAllCategories("ASP.NET");

            // assert
            Assert.Equal(3, categories.Data.Count);
            Assert.Equal(4, categories.Data[0].Id);
            Assert.Equal(2, categories.Data[1].Id);
            Assert.Equal(1, categories.Data[2].Id);
        }

        [Fact]
        public void GetCategoryById_WithCategoryIdZero_ShouldReturnNull()
        {
            // arrange
            var categoryRepositoryMock = new Mock<IRepository<BlogCategory>>();
            var cacheManagerMock = new Mock<IStaticCacheManager>();
            var blogCategoryService = new BlogCategoryService(categoryRepositoryMock.Object, cacheManagerMock.Object);

            // action
            var category = blogCategoryService.GetCategoryById(0);

            // assert
            Assert.Null(category);
        }

        [Fact]
        public void GetCategoriesByIds_WithCategoryIdsNull_ShouldReturnNoData()
        {
            // arrange
            var categoryRepositoryMock = new Mock<IRepository<BlogCategory>>();
            var cacheManagerMock = new Mock<IStaticCacheManager>();
            var blogCategoryService = new BlogCategoryService(categoryRepositoryMock.Object, cacheManagerMock.Object);

            // action
            var categories = blogCategoryService.GetCategoriesByIds(null);

            // assert
            Assert.NotNull(categories);
            Assert.Equal(0, categories.Count);
        }

        [Fact]
        public void GetFormattedBreadCrumb_WithCorrectParameter_ShouldReturnBreadCrumb()
        {
            // arrange
            var categoryRepositoryMock = new Mock<IRepository<BlogCategory>>();
            var cacheManagerMock = new Mock<IStaticCacheManager>();
            var blogCategoryService = new BlogCategoryService(categoryRepositoryMock.Object, cacheManagerMock.Object);
            var blogCategoryLevel1 = new BlogCategory()
            {
                Id = 1,
                Name = "ASP.NET",
                ParentCategoryId = 0
            };
            var blogCategoryLevel2 = new BlogCategory()
            {
                Id = 2,
                Name = "ASP.NET CORE",
                ParentCategoryId = 1
            };
            var blogCategoryLevel3 = new BlogCategory()
            {
                Id = 3,
                Name = "View Component",
                ParentCategoryId = 2
            };
            var allCategories = new List<BlogCategory> {blogCategoryLevel1, blogCategoryLevel2, blogCategoryLevel3};

            // action
            var breadCrumb = blogCategoryService.GetFormattedBreadCrumb(blogCategoryLevel3, allCategories, ">");

            // assert
            Assert.Equal("ASP.NET > ASP.NET CORE > View Component", breadCrumb);
        }

        [Fact]
        public void GetCategoryBreadCrumb_WithCorrectParameter_ShouldReturnByLevelSorted()
        {
            // arrange
            var categoryRepositoryMock = new Mock<IRepository<BlogCategory>>();
            var cacheManagerMock = new Mock<IStaticCacheManager>();
            var blogCategoryService = new BlogCategoryService(categoryRepositoryMock.Object, cacheManagerMock.Object);
            var blogCategoryLevel1 = new BlogCategory()
            {
                Id = 1,
                Name = "ASP.NET",
                ParentCategoryId = 0
            };
            var blogCategoryLevel2 = new BlogCategory()
            {
                Id = 2,
                Name = "ASP.NET CORE",
                ParentCategoryId = 1
            };
            var blogCategoryLevel3 = new BlogCategory()
            {
                Id = 3,
                Name = "View Component",
                ParentCategoryId = 2
            };
            var allCategories = new List<BlogCategory> { blogCategoryLevel1, blogCategoryLevel2, blogCategoryLevel3 };

            // action
            var breadCrumbs = blogCategoryService.GetCategoryBreadCrumb(blogCategoryLevel3, allCategories);

            // assert
            Assert.Equal(blogCategoryLevel1, breadCrumbs[0]);
            Assert.Equal(blogCategoryLevel2, breadCrumbs[1]);
            Assert.Equal(blogCategoryLevel3, breadCrumbs[2]);
        }

        [Fact]
        public void SortCategoriesForTree_WithCorrectParameter_ShouldReturnCategoriesSorted()
        {
            // arrange
            var categoryRepositoryMock = new Mock<IRepository<BlogCategory>>();
            var cacheManagerMock = new Mock<IStaticCacheManager>();
            var blogCategoryService = new BlogCategoryService(categoryRepositoryMock.Object, cacheManagerMock.Object);
            var blogCategoryLevel1 = new BlogCategory()
            {
                Id = 2,
                Name = "ASP.NET CORE",
                ParentCategoryId = 1
            };
            var blogCategoryLevel2 = new BlogCategory()
            {
                Id = 1,
                Name = "ASP.NET",
                ParentCategoryId = 0
            };
            var blogCategoryLevel3 = new BlogCategory()
            {
                Id = 3,
                Name = "View Component",
                ParentCategoryId = 2
            };

            var allCategories = new List<BlogCategory> { blogCategoryLevel1, blogCategoryLevel2, blogCategoryLevel3 };

            // action
            var breadCrumbs = blogCategoryService.SortCategoriesForTree(allCategories);

            // assert
            Assert.Equal(blogCategoryLevel2, breadCrumbs[0]);
            Assert.Equal(blogCategoryLevel1, breadCrumbs[1]);
            Assert.Equal(blogCategoryLevel3, breadCrumbs[2]);
        }
    }
}
