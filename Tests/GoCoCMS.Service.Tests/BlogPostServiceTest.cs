﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GoCoCMS.Service.Tests
{
    public class BlogPostServiceTest
    {
        [Fact]
        public void GetAllBlogPosts_HasBlogNameParameter_ShouldReturnHasData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                    {
                        new BlogPost
                        {
                            Id = 1,
                            Name = "Post1",
                            CreatedDate = It.IsAny<DateTime>()
                        },
                        new BlogPost
                        {
                            Id = 2,
                            Name = "Post2",
                            Deleted = true,
                            CreatedDate = It.IsAny<DateTime>()
                        }
                    }.AsQueryable()
            );
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetAllBlogPosts("Po", 1, Int32.MaxValue);

            // asert
            Assert.Single(blogPosts.Data);
        }

        [Fact]
        public void GetBlogPostById_WithBlogPostIdZero_ShouldReturnNull()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPost = blogPostService.GetBlogPostById(0);

            // asert
            Assert.Null(blogPost);
        }

        [Fact]
        public void GetBlogPostById_WithBlogPostIdCorrect_ShouldReturnData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.GetById(1))
                .Returns(new BlogPost
                    {
                        Id = 1,
                        Name = "Post1",
                        CreatedDate = It.IsAny<DateTime>()
                    }
                );
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPost = blogPostService.GetBlogPostById(1);

            // asert
            Assert.NotNull(blogPost);
        }

        [Fact]
        public void GetBlogPostsByIds_WithBlogPostIdsNull_ShouldReturnNoData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetBlogPostsByIds(null);

            // asert
            Assert.Equal(0, blogPosts.Count);
        }

        [Fact]
        public void GetBlogPostsByIds_WithHasBlogPostIds_ShouldReturnHasData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                        new BlogPost
                        {
                            Id = 1,
                            Name = "Post1",
                            CreatedDate = It.IsAny<DateTime>()
                        },
                        new BlogPost
                        {
                            Id = 2,
                            Name = "Post2",
                            CreatedDate = It.IsAny<DateTime>()
                        }
                    }.AsQueryable);
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetBlogPostsByIds(new []{1, 2});

            // asert
            Assert.Equal(2, blogPosts.Count);
        }

        [Fact]
        public void GetAllBlogPostsByBlogCategoryId_WithHasBlogPostIds_ShouldReturnHasData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                    new BlogPost
                    {
                        Id = 1,
                        Name = "Post1",
                        CreatedDate = It.IsAny<DateTime>()
                    },
                    new BlogPost
                    {
                        Id = 2,
                        Name = "Post2",
                        CreatedDate = It.IsAny<DateTime>()
                    }
                }.AsQueryable);
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetBlogPostsByIds(new[] { 1, 2 });

            // asert
            Assert.Equal(2, blogPosts.Count);
        }

        [Fact]
        public void GetAllBlogPostsByBlogCategoryIds_HasBlogCategoryIds_ShouldReturnHasData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                    new BlogPost
                    {
                        Id = 1,
                        Name = "Post1",
                        BlogCategoryId = 1,
                        CreatedDate = DateTime.Today.AddDays(-1)
                    },
                    new BlogPost
                    {
                        Id = 2,
                        Name = "Post2",
                        BlogCategoryId = 2,
                        CreatedDate = DateTime.Today
                    },
                    new BlogPost
                    {
                        Id = 3,
                        Name = "Post3",
                        BlogCategoryId = 3,
                        Deleted = true
                    }
                }.AsQueryable);
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetAllBlogPostsByBlogCategoryIds(new[] { 1, 2, 3 });

            // asert
            Assert.Equal(2, blogPosts.Data.Count);
            Assert.Equal(2, blogPosts.Data.First().Id);
            Assert.Equal(1, blogPosts.Data.Last().Id);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(2)]
        public void GetRecentPosts_WithNumberOfPost_ShouldReturnCorrect(int numberOfPost)
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                    new BlogPost
                    {
                        Id = 1,
                        Name = "Post1",
                        BlogCategoryId = 1,
                        CreatedDate = DateTime.Today.AddDays(-1)
                    },
                    new BlogPost
                    {
                        Id = 2,
                        Name = "Post2",
                        BlogCategoryId = 2,
                        CreatedDate = DateTime.Today
                    },
                    new BlogPost
                    {
                        Id = 3,
                        Name = "Post3",
                        BlogCategoryId = 3
                    }
                }.AsQueryable);
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetRecentPosts(numberOfPost);

            // asert
            Assert.Equal(numberOfPost, blogPosts.Count);
        }

        [Fact]
        public void GetPostsOnHomePage_WithNoBlogPostSetOnHomePage_ShouldReturnNoData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                    new BlogPost
                    {
                        Id = 1,
                        Name = "Post1"
                    }
                }.AsQueryable);
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetPostsOnHomePage();

            // asert
            Assert.Empty(blogPosts.Data);
        }

        [Fact]
        public void GetPostsOnHomePage_HasBlogPostSetOnHomePage_ShouldReturnHasData()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                    new BlogPost
                    {
                        Id = 1,
                        Name = "Post1",
                        ShowOnHomePage = true
                    }
                }.AsQueryable);
            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetPostsOnHomePage();

            // asert
            Assert.Single(blogPosts.Data);
        }

        [Fact]
        public void GetPostsByCategoryId_WithCategoryId_ShouldReturnCorrect()
        {
            // arrange
            var blogPostRepositoryMock = new Mock<IRepository<BlogPost>>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var pictureServiceMock = new Mock<IPictureService>();

            blogCategoryServiceMock.Setup(c => c.GetCategoryById(1))
                .Returns(new BlogCategory
                {
                    Id = 1,
                    Name = "Category1"
                });

            blogCategoryServiceMock.Setup(p => p.GetChildCategoryIds(1))
                .Returns(new List<int> { 1 });

            blogPostRepositoryMock.Setup(p => p.Table)
                .Returns(new List<BlogPost>
                {
                    new BlogPost
                    {
                        Id = 1,
                        Name = "Post1",
                        BlogCategoryId = 1
                    },
                    new BlogPost
                    {
                        Id = 2,
                        Name = "Post2",
                        BlogCategoryId = 1
                    },
                    new BlogPost
                    {
                        Id = 3,
                        Name = "Post3",
                        BlogCategoryId = 2
                    }
                }.AsQueryable);

            var blogPostService = new BlogPostService(blogPostRepositoryMock.Object,
                blogCategoryServiceMock.Object, pictureServiceMock.Object);

            // action
            var blogPosts = blogPostService.GetPostsByCategoryId(1);

            // asert
            Assert.Equal(2, blogPosts.Data.Count);
        }
    }
}
