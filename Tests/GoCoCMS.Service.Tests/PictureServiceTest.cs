﻿using GoCoCMS.Core.Helper.FileProviders;
using Moq;
using Xunit;

namespace GoCoCMS.Service.Tests
{
    public class PictureServiceTest
    {
        [Fact]
        public void SavePictureInFile_WithFileNameEmpty_ShouldBeOutFileName()
        {
            // arrange
            var fileProviderMock = new Mock<IFileProvider>();
            fileProviderMock.Setup(f => f.GetAbsolutePath(It.IsAny<string[]>())).Returns(It.IsAny<string>());
            fileProviderMock.Setup(f => f.WriteAllBytes(It.IsAny<string>(), It.IsAny<byte[]>()));

            var pictureService = new PictureService(fileProviderMock.Object);
            var fileName = string.Empty;

            // action
            pictureService.SavePictureInFile(It.IsAny<byte[]>(), "pjpeg", ref fileName);

            // assert
            Assert.NotEmpty(fileName);
        }
    }
}
