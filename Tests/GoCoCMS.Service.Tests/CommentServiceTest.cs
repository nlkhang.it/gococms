﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace GoCoCMS.Service.Tests
{
    public class CommentServiceTest
    {
        [Fact]
        public void GetById_WithIdIsZero_ShouldReturnNull()
        {
            // arrange
            var commentRepositoryMock = new Mock<IRepository<Comment>>();
            var commentService = new CommentService(commentRepositoryMock.Object);

            // action
            var comment = commentService.GetCommentById(0);

            // assert
            Assert.Null(comment);
        }

        [Fact]
        public void GetByIds_WithIdsIsNull_ShouldReturnNull()
        {
            // arrange
            var commentRepositoryMock = new Mock<IRepository<Comment>>();
            var commentService = new CommentService(commentRepositoryMock.Object);

            // action
            var comment = commentService.GetCommentsByIds(null);

            // assert
            Assert.IsType<List<Comment>>(comment);
        }

        [Fact]
        public void InsertUrlComment_WithUrlSlugNull_ShouldThrowException()
        {
            // arrange
            var commentRepositoryMock = new Mock<IRepository<Comment>>();
            var commentService = new CommentService(commentRepositoryMock.Object);

            // action
            Action<Comment> insertUrlSlugDel = commentService.InsertComment;

            // assert
            Assert.Throws<ArgumentNullException>(() => insertUrlSlugDel(null));
        }

        [Fact]
        public void UpdateUrlComment_WithUrlSlugNull_ShouldThrowException()
        {
            // arrange
            var commentRepositoryMock = new Mock<IRepository<Comment>>();
            var commentService = new CommentService(commentRepositoryMock.Object);

            // action
            Action<Comment> updateUrlSlugDel = commentService.UpdateComment;

            // assert
            Assert.Throws<ArgumentNullException>(() => updateUrlSlugDel(null));
        }

        [Fact]
        public void DeleteUrlComment_WithCommentNull_ShouldThrowException()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<Comment>>();
            var commentService = new CommentService(urlSlugRepositoryMock.Object);

            // action
            Action<Comment> deleteCommentDel = commentService.DeleteComment;

            // assert
            Assert.Throws<ArgumentNullException>(() => deleteCommentDel(null));
        }
    }
}
