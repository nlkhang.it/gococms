﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GoCoCMS.Service.Tests
{
    public class UrlSlugServiceTest
    {
        [Fact]
        public void DeleteUrlSlug_WithUrlSlugNull_ShouldThrowException()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            Action<UrlSlug> deleteUrlSlugDel = urlSlugService.DeleteUrlSlug;

            // assert
            Assert.Throws<ArgumentNullException>(() => deleteUrlSlugDel(null));
        }

        [Fact]
        public void DeleteUrlSlugs_WithUrlSlugsNull_ShouldThrowException()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            Action<IList<UrlSlug>> deleteUrlSlugsDel = urlSlugService.DeleteUrlSlugs;

            // assert
            Assert.Throws<ArgumentNullException>(() => deleteUrlSlugsDel(null));
        }

        [Fact]
        public void GetUrlSlugById_WithUrlSlugIdZero_ShouldReturnNull()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            var urlSlug = urlSlugService.GetUrlSlugById(0);

            // assert
            Assert.Null(urlSlug);
        }

        [Fact]
        public void GetUrlSlugsByIds_HasUrlSlugIds_ShouldReturnHasData()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            urlSlugRepositoryMock.Setup(u => u.Table)
                .Returns(new List<UrlSlug>
                {
                    new UrlSlug()
                    {
                        Id = 1,
                        Slug = "test-slug-1"
                    },
                    new UrlSlug()
                    {
                        Id = 2,
                        Slug = "test-slug-2"
                    },
                    new UrlSlug()
                    {
                        Id = 3,
                        Slug = "test-slug-3"
                    }
                }.AsQueryable());

            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            var urlSlugs = urlSlugService.GetUrlSlugsByIds(new []{ 1, 2 });

            // assert
            Assert.Equal(2, urlSlugs.Count);
        }

        [Fact]
        public void InsertUrlSlug_WithUrlSlugNull_ShouldThrowException()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            Action<UrlSlug> insertUrlSlugDel = urlSlugService.InsertUrlSlug;

            // assert
            Assert.Throws<ArgumentNullException>(() => insertUrlSlugDel(null));
        }

        [Fact]
        public void UpdateUrlSlug_WithUrlSlugNull_ShouldThrowException()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            Action<UrlSlug> updateUrlSlugDel = urlSlugService.UpdateUrlSlug;

            // assert
            Assert.Throws<ArgumentNullException>(() => updateUrlSlugDel(null));
        }

        [Fact]
        public void GetBySlug_WithSlugIsEmpty_ShouldReturnNull()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            var urlSlug = urlSlugService.GetBySlug(string.Empty);

            // assert
            Assert.Null(urlSlug);
        }

        [Theory]
        [InlineData("test-slug-1")]
        [InlineData("test-slug-2")]
        [InlineData("test-slug-3")]
        public void GetBySlug_WithCorrectSlug_ShouldReturnHasData(string slug)
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            urlSlugRepositoryMock.Setup(u => u.Table)
                .Returns(new List<UrlSlug>
                {
                    new UrlSlug()
                    {
                        Id = 1,
                        Slug = "test-slug-1"
                    },
                    new UrlSlug()
                    {
                        Id = 2,
                        Slug = "test-slug-2"
                    },
                    new UrlSlug()
                    {
                        Id = 3,
                        Slug = "test-slug-3"
                    }
                }.AsQueryable());

            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            var urlSlug = urlSlugService.GetBySlug(slug);

            // assert
            Assert.Equal(slug, urlSlug.Slug);
        }

        [Fact]
        public void GetSlug_WithGenericOfT_ShouldReturnWithString()
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            urlSlugRepositoryMock.Setup(u => u.Table)
                .Returns(new List<UrlSlug>
                {
                    new UrlSlug()
                    {
                        Id = 1,
                        EntityId = 11,
                        EntityName = "BlogPost",
                        Slug = "test-slug-1"
                    },
                    new UrlSlug()
                    {
                        Id = 2,
                        EntityId = 12,
                        EntityName = "BlogPost",
                        Slug = "test-slug-2"
                    },
                    new UrlSlug()
                    {
                        Id = 3,
                        EntityId = 13,
                        EntityName = "BlogPost",
                        Slug = "test-slug-3"
                    }
                }.AsQueryable());

            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);


            // action
            var urlSlug = urlSlugService.GetSlug(new BlogPost()
            {
                Id = 11,
                Name = "Test1"
            });

            // assert
            Assert.Equal("test-slug-1", urlSlug);
        }

        [Theory]
        [InlineData("test slug 1", "test-slug-1")]
        [InlineData("test slug. This is my slug", "test-slug-this-is-my-slug")]
        public void GetSeName_WithStringName_ShouldBeReturnSlugString(string nameInput, string slugOutput)
        {
            // arrange
            var urlSlugRepositoryMock = new Mock<IRepository<UrlSlug>>();
            var urlSlugService = new UrlSlugService(urlSlugRepositoryMock.Object);

            // action
            var urlSlug = urlSlugService.GetSeName(nameInput, true, false);

            // assert
            Assert.Equal(slugOutput, urlSlug);
        }
    }
}
