﻿using GoCoCMS.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace GoCoCMS.Web.Tests.Admin.Controllers
{
    public class HomeControllerTest
    {
        [Fact]
        public void IndexMethodGet_ShouldBeRedirectToCategoryIndex()
        {
            // arrange
            var homeController = new HomeController();

            // action
            var result = homeController.Index() as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal("Index", result.ActionName);
            Assert.Equal("Category", result.ControllerName);
        }
    }
}
