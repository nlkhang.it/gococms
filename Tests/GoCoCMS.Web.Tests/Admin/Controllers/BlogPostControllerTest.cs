﻿using GoCoCMS.Base.Tests;
using GoCoCMS.Core.Helper.FileProviders;
using GoCoCMS.Data.Domain;
using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Controllers;
using GoCoCMS.Web.Areas.Admin.Factories;
using GoCoCMS.Web.Areas.Admin.Models.Post;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GoCoCMS.Web.Tests.Admin.Controllers
{
    public class BlogPostControllerTest : IClassFixture<ControllerTestBase>
    {
        [Fact]
        public void Index_WithMethodGet_ShouldReturnViewModel()
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostListModel(It.IsAny<BlogPostSearchModel>(), 1, 10))
                .Returns(new BlogPostListModel());

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            // action
            var result = blogPostController.Index(new BlogPostSearchModel()) as ViewResult;

            // assert
            Assert.IsType<BlogPostListModel>(result?.Model);
        }

        [Fact]
        public void CreateMethodGet_ShouldReturnViewModel()
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostModel(It.IsAny<BlogPostModel>(), null))
                .Returns(new BlogPostModel());

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            // action
            var result = blogPostController.Create() as ViewResult;

            // assert
            Assert.IsType<BlogPostModel>(result?.Model);
        }

        [Fact]
        public void CreateMethodPost_ModelStateInvalid_ShouldReturnBlogPostModel()
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostModel(It.IsAny<BlogPostModel>(), null))
                .Returns(new BlogPostModel());

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            blogPostController.ModelState.AddModelError("Required", "Missing required field");

            // action
            var result = blogPostController.Create(new BlogPostModel(), false) as ViewResult;

            // assert
            Assert.IsType<BlogPostModel>(result?.Model);
        }

        [Theory]
        [InlineData(false, "Index")]
        [InlineData(true, "Create")]
        public void CreateMethodPost_ModelStateValid_ShouldBeRedirectCorrectView(bool continueEditing, string actionResult)
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostModel(It.IsAny<BlogPostModel>(), null))
                .Returns(new BlogPostModel());

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            // action
            var result = blogPostController.Create(new BlogPostModel(), continueEditing) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal(actionResult, result.ActionName);
        }

        [Fact]
        public void EditMethodPost_ModelStateInvalid_ShouldReturnBlogPostModel()
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            var model = new BlogPostModel { Id = 1 };
            var blogPost = new BlogPost {Id = 1};

            blogPostServiceMock.Setup(p => p.GetBlogPostById(It.IsAny<int>())).Returns(blogPost);
            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostModel(model, blogPost))
                .Returns(model);

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            blogPostController.ModelState.AddModelError("Required", "Missing required field");

            // action
            var result = blogPostController.Edit(model, false) as ViewResult;

            // assert
            Assert.IsType<BlogPostModel>(result?.Model);
        }

        [Fact]
        public void EditMethodPost_ModelValidAndBlogPostNull_ShouldRedirectToActionIndex()
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostModel(It.IsAny<BlogPostModel>(), null))
                .Returns(new BlogPostModel());

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);
            // action
            var result = blogPostController.Edit(new BlogPostModel(), false) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal("Index", result.ActionName);
        }

        [Theory]
        [InlineData(false, "Index")]
        [InlineData(true, "Edit")]
        public void EditMethodPost_ModelStateValid_ShouldBeRedirectCorrectView(bool continueEditing, string actionResult)
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            blogPostModelFactoryMock.Setup(p => p.PrepareBlogPostModel(It.IsAny<BlogPostModel>(), null))
                .Returns(new BlogPostModel());

            var model = new BlogPostModel { Id = 1};
            blogPostServiceMock.Setup(p => p.GetBlogPostById(model.Id)).Returns(new BlogPost { Id = 1});

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            // action
            var result = blogPostController.Edit(model, continueEditing) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal(actionResult, result.ActionName);
        }

        [Fact]
        public void DeleteMethodGet_CanDeleteBlogPost()
        {
            // arrange
            var blogPostModelFactoryMock = new Mock<IBlogPostModelFactory>();
            var blogPostServiceMock = new Mock<IBlogPostService>();
            var fileProviderMock = new Mock<IFileProvider>();
            var pictureServiceMock = new Mock<IPictureService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();
            var userManagerMock = IdentityMockHelper.MockUserManager<Data.Domain.Identity.User>();

            var blogPostController = new BlogPostController(blogPostModelFactoryMock.Object,
                blogPostServiceMock.Object, fileProviderMock.Object,
                pictureServiceMock.Object, urlSlugServiceMock.Object,
                userManagerMock.Object);

            // action
            var result = blogPostController.Delete(1) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal("Index", result.ActionName);
        }
    }
}
