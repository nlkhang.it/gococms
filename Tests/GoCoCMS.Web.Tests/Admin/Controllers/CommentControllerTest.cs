﻿using GoCoCMS.Base.Tests;
using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Controllers;
using GoCoCMS.Web.Areas.Admin.Factories;
using GoCoCMS.Web.Areas.Admin.Models.Comment;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GoCoCMS.Web.Tests.Admin.Controllers
{
    public class CommentControllerTest : IClassFixture<ControllerTestBase>
    {
        [Fact]
        public void Index_WithMethodGet_ShouldReturnViewModel()
        {
            // arrange
            var blogCommentModelFactoryMock = new Mock<IBlogCommentModelFactory>();
            var commentServiceMock = new Mock<ICommentService>();

            blogCommentModelFactoryMock.Setup(p => p.PrepareCommentListModel(It.IsAny<CommentSearchModel>(), 1, 10))
                .Returns(new CommentListModel());

            var commentController = new CommentController(blogCommentModelFactoryMock.Object,
                commentServiceMock.Object);

            // action
            var result = commentController.Index(new CommentSearchModel()) as ViewResult;

            // assert
            Assert.IsType<CommentListModel>(result?.Model);
        }

        [Fact]
        public void DeleteMethodGet_CanDeleteComment()
        {
            // arrange
            var blogCommentModelFactoryMock = new Mock<IBlogCommentModelFactory>();
            var commentServiceMock = new Mock<ICommentService>();

            var commentController = new CommentController(blogCommentModelFactoryMock.Object,
                commentServiceMock.Object);

            // action
            var result = commentController.Delete(1) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal("Index", result.ActionName);
        }
    }
}
