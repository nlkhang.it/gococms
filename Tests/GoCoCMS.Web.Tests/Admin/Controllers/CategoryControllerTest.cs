﻿using GoCoCMS.Base.Tests;
using GoCoCMS.Data.Domain;
using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Controllers;
using GoCoCMS.Web.Areas.Admin.Factories;
using GoCoCMS.Web.Areas.Admin.Models.Category;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GoCoCMS.Web.Tests.Admin.Controllers
{
    public class CategoryControllerTest : IClassFixture<ControllerTestBase>
    {
        [Fact]
        public void Index_WithMethodGet_ShouldReturnViewModel()
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryListModel(It.IsAny<CategorySearchModel>(), 1, 10))
                .Returns(new CategoryListModel());

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            // action
            var result = categoryController.Index(new CategorySearchModel()) as ViewResult;

            // assert
            Assert.IsType<CategoryListModel>(result?.Model);
        }

        [Fact]
        public void Create_WithMethodGet_ShouldReturnViewModel()
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryModel(It.IsAny<CategoryModel>(), null))
                .Returns(new CategoryModel());

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            // action
            var result = categoryController.Create() as ViewResult;

            // assert
            Assert.IsType<CategoryModel>(result?.Model);
        }

        [Fact]
        public void CreateMethodPost_ModelStateInvalid_ShouldReturnBlogPostModel()
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryModel(It.IsAny<CategoryModel>(), null))
                .Returns(new CategoryModel());

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            categoryController.ModelState.AddModelError("Required", "Missing required field");

            // action
            var result = categoryController.Create(new CategoryModel(), false) as ViewResult;

            // assert
            Assert.IsType<CategoryModel>(result?.Model);
        }

        [Theory]
        [InlineData(false, "Index")]
        [InlineData(true, "Create")]
        public void CreateMethodPost_ModelStateValid_ShouldBeRedirectCorrectView(bool continueEditing, string actionResult)
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryModel(It.IsAny<CategoryModel>(), null))
                .Returns(new CategoryModel());

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            // action
            var result = categoryController.Create(new CategoryModel(), continueEditing) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal(actionResult, result.ActionName);
        }

        [Fact]
        public void EditMethodPost_ModelStateInvalid_ShouldReturnBlogPostModel()
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            var model = new CategoryModel { Id = 1 };
            var blogCategory= new BlogCategory { Id = 1 };

            blogCategoryServiceMock.Setup(p => p.GetCategoryById(It.IsAny<int>())).Returns(blogCategory);
            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryModel(model, blogCategory))
                .Returns(model);

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            categoryController.ModelState.AddModelError("Required", "Missing required field");

            // action
            var result = categoryController.Edit(model, false) as ViewResult;

            // assert
            Assert.IsType<CategoryModel>(result?.Model);
        }

        [Fact]
        public void EditMethodPost_ModelValidAndBlogPostNull_ShouldRedirectToActionIndex()
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryModel(It.IsAny<CategoryModel>(), null))
                .Returns(new CategoryModel());

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            // action
            var result = categoryController.Edit(new CategoryModel(), false) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal("Index", result.ActionName);
        }

        [Theory]
        [InlineData(false, "Index")]
        [InlineData(true, "Edit")]
        public void EditMethodPost_ModelStateValid_ShouldBeRedirectCorrectView(bool continueEditing, string actionResult)
        {
            // arrange
            var blogCategoryModelFactoryMock = new Mock<IBlogCategoryModelFactory>();
            var blogCategoryServiceMock = new Mock<IBlogCategoryService>();
            var urlSlugServiceMock = new Mock<IUrlSlugService>();

            blogCategoryModelFactoryMock.Setup(p => p.PrepareCategoryModel(It.IsAny<CategoryModel>(), null))
                .Returns(new CategoryModel());

            var model = new CategoryModel { Id = 1 };
            blogCategoryServiceMock.Setup(p => p.GetCategoryById(model.Id)).Returns(new BlogCategory { Id = 1 });

            var categoryController = new CategoryController(blogCategoryModelFactoryMock.Object,
                blogCategoryServiceMock.Object,
                urlSlugServiceMock.Object);

            // action
            var result = categoryController.Edit(model, continueEditing) as RedirectToActionResult;

            // assert
            Assert.NotNull(result);
            Assert.Equal(actionResult, result.ActionName);
        }
    }
}
