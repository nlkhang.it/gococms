﻿using GoCoCMS.Web.Controllers;
using GoCoCMS.Web.Factories;
using GoCoCMS.Web.Models.Post;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GoCoCMS.Web.Tests.User.Controllers
{
    public class HomeControllerTest
    {
        [Fact]
        public void IndexMethodGet_ShouldBeReturnViewModel()
        {
            // arrange
            var postModelFactoryMock = new Mock<IPostModelFactory>();

            postModelFactoryMock.Setup(p => p.PrepareHomePagePostListModel(1, 10))
                .Returns(new BlogPostListModel());

            var homeController = new HomeController(postModelFactoryMock.Object);

            // action
             var result = homeController.Index(1, 10) as ViewResult;

            // assert
            Assert.NotNull(result);
            Assert.IsType<BlogPostListModel>(result.Model);
        }
    }
}
