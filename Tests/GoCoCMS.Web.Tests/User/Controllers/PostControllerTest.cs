﻿using GoCoCMS.Web.Controllers;
using GoCoCMS.Web.Factories;
using GoCoCMS.Web.Models.Post;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GoCoCMS.Web.Tests.User.Controllers
{
    public class PostControllerTest
    {
        [Fact]
        public void PostsByCategoryMethodGet_WithCategoryId_ShouldBeReturnViewResult()
        {
            // arrange
            var postModelFactoryMock = new Mock<IPostModelFactory>();

            postModelFactoryMock.Setup(p => p.PreparePostsByCategoryModel(1, 1, 10))
                .Returns(new BlogPostListModel());

            var postController = new PostController(postModelFactoryMock.Object);

            // action
            var result = postController.PostsByCategory(1) as ViewResult;

            // assert
            Assert.NotNull(result);
            Assert.IsType<BlogPostListModel>(result.Model);
        }

        [Fact]
        public void DetailsMethodGet_WithId_ShouldBeReturnViewResult()
        {
            // arrange
            var postModelFactoryMock = new Mock<IPostModelFactory>();

            postModelFactoryMock.Setup(p => p.PreparePostModelById(1))
                .Returns(new PostModel());

            var postController = new PostController(postModelFactoryMock.Object);

            // action
            var result = postController.Details(1) as ViewResult;

            // assert
            Assert.NotNull(result);
            Assert.IsType<PostModel>(result.Model);
        }
    }
}
