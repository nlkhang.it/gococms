﻿using GoCoCMS.Data.Domain;
using Microsoft.EntityFrameworkCore;

namespace GoCoCMS.Data
{
    public interface IDbContext
    {
        #region Methods

        DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;

        int SaveChanges();

        #endregion
    }
}
