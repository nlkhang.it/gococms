﻿// <auto-generated />
using System;
using GoCoCMS.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace GoCoCMS.Data.Migrations
{
    [DbContext(typeof(GoCoCmsContext))]
    [Migration("20181027164919_add-foreign-key")]
    partial class addforeignkey
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GoCoCMS.Data.Domain.BlogCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Deleted");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<int>("DisplayOrder");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("ParentCategoryId");

                    b.HasKey("Id");

                    b.ToTable("BlogCategories");
                });

            modelBuilder.Entity("GoCoCMS.Data.Domain.BlogPost", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BlogCategoryId");

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<string>("ContentOverview")
                        .IsRequired();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Deleted");

                    b.Property<DateTime?>("EndDate");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime?>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("BlogCategoryId");

                    b.ToTable("BlogPosts");
                });

            modelBuilder.Entity("GoCoCMS.Data.Domain.BlogPost", b =>
                {
                    b.HasOne("GoCoCMS.Data.Domain.BlogCategory", "BlogCategory")
                        .WithMany("BlogPosts")
                        .HasForeignKey("BlogCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
