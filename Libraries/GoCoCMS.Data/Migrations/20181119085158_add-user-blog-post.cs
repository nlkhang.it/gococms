﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GoCoCMS.Data.Migrations
{
    public partial class adduserblogpost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserIdCreated",
                table: "BlogPosts",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_UserIdCreated",
                table: "BlogPosts",
                column: "UserIdCreated");

            migrationBuilder.AddForeignKey(
                name: "FK_BlogPosts_Users_UserIdCreated",
                table: "BlogPosts",
                column: "UserIdCreated",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlogPosts_Users_UserIdCreated",
                table: "BlogPosts");

            migrationBuilder.DropIndex(
                name: "IX_BlogPosts_UserIdCreated",
                table: "BlogPosts");

            migrationBuilder.DropColumn(
                name: "UserIdCreated",
                table: "BlogPosts");
        }
    }
}
