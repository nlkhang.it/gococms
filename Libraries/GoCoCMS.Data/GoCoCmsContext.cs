﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GoCoCMS.Data
{
    public class GoCoCmsContext : IdentityDbContext<User, 
        Role, 
        long, 
        IdentityUserClaim<long>, 
        UserRole, 
        IdentityUserLogin<long>, 
        IdentityRoleClaim<long>, 
        IdentityUserToken<long>>, 
        IDbContext
    {
        #region Ctor

        public GoCoCmsContext(DbContextOptions<GoCoCmsContext> options) : base(options)
        {
        }

        #endregion

        #region Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Blog
            modelBuilder.Entity<BlogCategory>().ToTable("BlogCategories");
            modelBuilder.Entity<BlogPost>().ToTable("BlogPosts");
            modelBuilder.Entity<UrlSlug>().ToTable("UrlSlugs");
            modelBuilder.Entity<Comment>().ToTable("Comments");

            // Identity
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserClaim<long>>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserLogin<long>>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityRoleClaim<long>>().ToTable("RoleClaims");
            modelBuilder.Entity<IdentityUserToken<long>>().ToTable("UserTokens");
        }

        public new virtual DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        #endregion
    }
}
