﻿namespace GoCoCMS.Data.Domain
{
    public class UrlSlug : BaseEntity
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string Slug { get; set; }
    }
}
