﻿namespace GoCoCMS.Data.Domain
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
