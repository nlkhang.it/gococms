﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoCoCMS.Data.Domain
{
    public class Comment : BaseEntity
    {
        [Required]
        public string Content { get; set; }

        [Required]
        public int BlogPostId { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public bool Deleted { get; set; }


        [ForeignKey(nameof(BlogPostId))]
        public virtual BlogPost BlogPost { get; set; }
    }
}
