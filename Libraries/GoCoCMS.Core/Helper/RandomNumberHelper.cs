﻿using System;
using System.Security.Cryptography;

namespace GoCoCMS.Core.Helper
{
    public static class RandomNumberHelper
    {
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new System.Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }
    }
}
