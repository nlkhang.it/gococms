﻿using System.Linq;

namespace GoCoCMS.Core.Paging
{
    public static class PagingExtension
    {
        public static IPagedList<T> ToPagedList<T>(this IQueryable<T> query, int pageIndex, int pageSize)
        {
            return new PagedList<T>(query, pageIndex, pageSize);
        }
    }
}
