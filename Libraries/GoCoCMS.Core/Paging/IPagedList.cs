﻿using System.Collections.Generic;

namespace GoCoCMS.Core.Paging
{
    public interface IPagedList<T>
    {
        List<T> Data { get; set; }
        int PageIndex { get; set; }
        int PageSize { get; set; }
        int TotalItems { get; set; }
        int TotalPages { get; set; }
        bool HasPreviousPage { get; set; }
        bool HasNextPage { get; set; }
    }
}
