﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GoCoCMS.Core.Paging
{
    public class PagedList<T> : IPagedList<T>
    {
        public PagedList(IQueryable<T> query, int pageIndex, int pageSize)
        {
            if (pageIndex == 0)
                pageIndex = 1;

            if (pageSize == 0)
                pageSize = 10;

            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalItems = query.Count();
            TotalPages = TotalItems > 0 ? (int)Math.Ceiling((double)TotalItems / pageSize) : 0;
            HasPreviousPage = pageIndex > 1;
            HasNextPage = pageIndex < TotalPages;
            Data = query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
        }

        public List<T> Data { get; set; }
        public int PageIndex { set; get; }
        public int PageSize { set; get; }
        public int TotalItems { set; get; }
        public int TotalPages { set; get; }
        public bool HasPreviousPage { set; get; }
        public bool HasNextPage { set; get; }
    }
}
