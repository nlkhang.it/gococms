﻿namespace GoCoCMS.Core.Caching
{
    public static class CachingDefaults
    {
        /// <summary>
        /// Gets the default cache time in minutes
        /// </summary>
        public static int CacheTime => 60;

        public static string RedisDataProtectionKey => "DataProtectionKeys";
    }
}
