﻿using StackExchange.Redis;
using System;
using System.Net;

namespace GoCoCMS.Core.Caching
{
    public interface IRedisConnectionWrapper : IDisposable
    {
        IDatabase GetDatabase(int? db = null);

        IServer GetServer(EndPoint endPoint);

        EndPoint[] GetEndPoints();

        void FlushDatabase(int? db = null);
    }
}
