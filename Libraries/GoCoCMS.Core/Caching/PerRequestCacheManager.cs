﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GoCoCMS.Core.Caching
{
    public partial class PerRequestCacheManager : ICacheManager
    {
        #region Fields

        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Ctor

        public PerRequestCacheManager(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Utilities

        protected virtual IDictionary<object, object> GetItems()
        {
            return _httpContextAccessor.HttpContext?.Items;
        }

        #endregion

        #region Methods

        public virtual T Get<T>(string key, Func<T> acquire, int? cacheTime = null)
        {
            var items = GetItems();
            if (items == null)
                return acquire();

            //item already is in cache, so return it
            if (items[key] != null)
                return (T)items[key];

            //or create it using passed function
            var result = acquire();

            //and set in cache (if cache time is defined)
            if (result != null && (cacheTime ?? CachingDefaults.CacheTime) > 0)
                items[key] = result;

            return result;
        }

        public virtual void Set(string key, object data, int cacheTime)
        {
            var items = GetItems();
            if (items == null)
                return;

            if (data != null)
                items[key] = data;
        }

        public virtual bool IsSet(string key)
        {
            var items = GetItems();

            return items?[key] != null;
        }

        public virtual void Remove(string key)
        {
            var items = GetItems();

            items?.Remove(key);
        }

        public virtual void RemoveByPattern(string pattern)
        {
            var items = GetItems();
            if (items == null)
                return;

            //get cache keys that matches pattern
            var regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var matchesKeys = items.Keys.Select(p => p.ToString()).Where(key => regex.IsMatch(key)).ToList();

            //remove matching values
            foreach (var key in matchesKeys)
            {
                items.Remove(key);
            }
        }

        public virtual void Clear()
        {
            var items = GetItems();

            items?.Clear();
        }

        public virtual void Dispose()
        {
            //nothing special
        }

        #endregion
    }
}
