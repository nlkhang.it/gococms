﻿namespace GoCoCMS.Core.Caching
{
    public static class CacheKeyDefault
    {
        // Category
        public static string CategoryByIdCacheKey => "category.id-{0}";
        public static string AllCategoriesByParentIdCacheKey => "all.category.byparentid-{0}";
        public static string ChildCategoriesIdByParentIdCacheKey => "childcategoryids.byparentid-{0}";
        public static string ChildCategoriesByParentIdCacheKey => "childcategory.byparentid-{0}";
        public static string AllCategoriesCacheKey => "allcategories";

        // post

    }
}
