﻿namespace GoCoCMS.Core.Web
{
    public interface IWebHelper
    {
        string GetThisPageUrl(bool includeQueryString, bool lowercaseUrl = false);
        string ModifyQueryString(string url, string key, params string[] values);
    }
}
