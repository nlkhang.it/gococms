﻿namespace GoCoCMS.Service
{
    public interface IPictureService
    {
        void SavePictureInFile(byte[] pictureBinary, string mimeType, ref string fileName);
        void DeletePicture(string fileName);
    }
}
