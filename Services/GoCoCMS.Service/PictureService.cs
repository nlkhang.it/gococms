﻿using GoCoCMS.Core.Helper;
using GoCoCMS.Core.Helper.FileProviders;

namespace GoCoCMS.Service
{
    public class PictureService : IPictureService
    {
        #region Fields

        private readonly IFileProvider _fileProvider;

        #endregion

        #region Ctor

        public PictureService(IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
        }

        #endregion

        #region Methods

        public void SavePictureInFile(byte[] pictureBinary, string mimeType, ref string fileName)
        {
            var random = RandomNumberHelper.GenerateRandomInteger();
            var fileExtension = GetFileExtensionFromMimeType(mimeType);
            fileName = $"{random}.{fileExtension}";
            var absolutePath = _fileProvider.GetAbsolutePath("images", fileName);
            _fileProvider.WriteAllBytes(absolutePath, pictureBinary);
        }

        public void DeletePicture(string fileName)
        {
            var absolutePath = _fileProvider.GetAbsolutePath("images", fileName);
            _fileProvider.DeleteFile(absolutePath);
        }

        #endregion

        #region Utilities

        protected virtual string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            //TODO use FileExtensionContentTypeProvider to get file extension

            var parts = mimeType.Split('/');
            var lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }

            return lastPart;
        }

        #endregion

    }
}
