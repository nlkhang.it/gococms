﻿using GoCoCMS.Core.Paging;
using GoCoCMS.Data.Domain;
using System.Collections.Generic;

namespace GoCoCMS.Service
{
    public interface IBlogPostService
    {
        IPagedList<BlogPost> GetAllBlogPosts(string blogName, int pageIndex = 1, int pageSize = 10);
        BlogPost GetBlogPostById(int blogPostId);
        IList<BlogPost> GetBlogPostsByIds(int[] blogPostIds);
        IList<BlogPost> GetAllBlogPostsByBlogCategoryId(int blogCategoryId);
        IPagedList<BlogPost> GetAllBlogPostsByBlogCategoryIds(int[] blogCategoryIds, int pageIndex = 1, int pageSize = 10);
        IList<BlogPost> GetRecentPosts(int numberOfPost);
        IPagedList<BlogPost> GetPostsOnHomePage(int pageIndex = 1, int pageSize = 10);
        IPagedList<BlogPost> GetPostsByCategoryId(int categoryId, int pageIndex = 1, int pageSize = 10);
        void InsertBlogPost(BlogPost blogPost);
        void UpdateBlogPost(BlogPost blogPost);
        void DeleteBlogPost(BlogPost blogPost);
        IPagedList<BlogPost> GetBlogPostsByTerms(string terms, int pageIndex = 1, int pageSize = 10);
    }
}
