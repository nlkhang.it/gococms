﻿using GoCoCMS.Core.Paging;
using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoCoCMS.Service
{
    public class CommentService : ICommentService
    {
        #region Fields

        private readonly IRepository<Comment> _commentRepository;

        #endregion

        #region Ctor

        public CommentService(IRepository<Comment> commentRepository)
        {
            _commentRepository = commentRepository;
        }

        #endregion

        public IPagedList<Comment> GetAllComments(string content = null, int pageIndex = 1, int pageSize = 10)
        {
            var query = _commentRepository.Table;
            if (!string.IsNullOrWhiteSpace(content))
                query = query.Where(p => p.Content.Contains(content));

            query = query.Where(p => !p.Deleted);
            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToPagedList(pageIndex, pageSize);
        }

        public Comment GetCommentById(int commentId)
        {
            if (commentId == 0)
                return null;

            return _commentRepository.GetById(commentId);
        }

        public IList<Comment> GetCommentByPostId(int postId)
        {
            var query = _commentRepository.Table.Where(p => !p.Deleted && p.BlogPostId == postId);

            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToList();
        }

        public IList<Comment> GetCommentsByIds(int[] commentIds)
        {
            if (commentIds == null || commentIds.Length == 0)
                return new List<Comment>();

            var query = _commentRepository.Table.Where(p => commentIds.Contains(p.Id) && !p.Deleted);
            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToList();
        }

        public void InsertComment(Comment comment)
        {
            if (comment == null)
                throw new ArgumentNullException(nameof(comment));

            comment.CreatedDate = DateTime.Now;
            _commentRepository.Insert(comment);
        }

        public void UpdateComment(Comment comment)
        {
            if (comment == null)
                throw new ArgumentNullException(nameof(comment));

            _commentRepository.Update(comment);
        }

        public void DeleteComment(Comment comment)
        {
            if (comment == null)
                throw new ArgumentNullException(nameof(comment));

            comment.Deleted = true;
            _commentRepository.Update(comment);
        }
    }
}
