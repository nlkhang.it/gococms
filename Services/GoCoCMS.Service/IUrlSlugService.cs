﻿using GoCoCMS.Data.Domain;
using System.Collections.Generic;

namespace GoCoCMS.Service
{
    public interface IUrlSlugService
    {
        void DeleteUrlSlug(UrlSlug urlSlug);

        void DeleteUrlSlugs(IList<UrlSlug> urlSlugs);

        UrlSlug GetUrlSlugById(int urlSlugId);

        IList<UrlSlug> GetUrlSlugsByIds(int[] urlSlugIds);

        void InsertUrlSlug(UrlSlug urlSlug);

        void UpdateUrlSlug(UrlSlug urlSlug);

        UrlSlug GetBySlug(string slug);

        string GetSlug<T>(T entity) where T : BaseEntity;
        string GetSlug(int entityId, string entityName);

        void SaveSlug<T>(T entity, string slug) where T : BaseEntity;

        string GetSeName(string name, bool convertNonWesternChars, bool allowUnicodeCharsInUrls);
        string ValidateSeName<T>(T entity, string seName, string name, bool ensureNotEmpty) where T : BaseEntity;
        string ValidateSeName(int entityId, string entityName, string seName, string name, bool ensureNotEmpty);
    }
}
