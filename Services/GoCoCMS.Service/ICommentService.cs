﻿using GoCoCMS.Core.Paging;
using GoCoCMS.Data.Domain;
using System.Collections.Generic;

namespace GoCoCMS.Service
{
    public interface ICommentService
    {
        IPagedList<Comment> GetAllComments(string content = null, int pageIndex = 1, int pageSize = 10);
        Comment GetCommentById(int commentId);
        IList<Comment> GetCommentByPostId(int postId);
        IList<Comment> GetCommentsByIds(int[] commentIds);
        void InsertComment(Comment comment);
        void UpdateComment(Comment comment);
        void DeleteComment(Comment comment);
    }
}
