﻿using GoCoCMS.Core.Paging;
using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoCoCMS.Service
{
    public class BlogPostService : IBlogPostService
    {
        #region Fields

        private readonly IRepository<BlogPost> _blogPostRepository;
        private readonly IBlogCategoryService _blogCategoryService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public BlogPostService(IRepository<BlogPost> blogPostRepository,
            IBlogCategoryService blogCategoryService,
            IPictureService pictureService)
        {
            _blogPostRepository = blogPostRepository;
            _blogCategoryService = blogCategoryService;
            _pictureService = pictureService;
        }

        #endregion

        #region Methods

        public IPagedList<BlogPost> GetAllBlogPosts(string blogName, int pageIndex = 1, int pageSize = 10)
        {
            var query = _blogPostRepository.Table;
            if (!string.IsNullOrWhiteSpace(blogName))
                query = query.Where(p => p.Name.Contains(blogName));

            query = query.Where(p => !p.Deleted);
            query = query.OrderByDescending(p => p.CreatedDate);            

            return query.ToPagedList(pageIndex, pageSize);
        }

        public BlogPost GetBlogPostById(int blogPostId)
        {
            if (blogPostId == 0)
                return null;

            return _blogPostRepository.GetById(blogPostId);
        }

        public IList<BlogPost> GetBlogPostsByIds(int[] blogPostIds)
        {
            if (blogPostIds == null || blogPostIds.Length == 0)
                return new List<BlogPost>();

            var query = _blogPostRepository.Table.Where(p => blogPostIds.Contains(p.Id) && !p.Deleted);
            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToList();
        }

        public IList<BlogPost> GetAllBlogPostsByBlogCategoryId(int blogCategoryId)
        {
            var query = _blogPostRepository.Table.Where(p => p.BlogCategoryId == blogCategoryId);
            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToList();
        }

        public IPagedList<BlogPost> GetAllBlogPostsByBlogCategoryIds(int[] blogCategoryIds, int pageIndex = 1, int pageSize = 10)
        {
            var query = _blogPostRepository.Table.Where(p => blogCategoryIds.Contains(p.BlogCategoryId)
            && !p.Deleted);
            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToPagedList(pageIndex, pageSize);
        }

        public IList<BlogPost> GetRecentPosts(int numberOfPost)
        {
            var query = _blogPostRepository.Table.Where(p => !p.Deleted);
            query = query.Take(numberOfPost).OrderByDescending(p => p.CreatedDate);

            return query.ToList();
        }

        public IPagedList<BlogPost> GetPostsOnHomePage(int pageIndex = 1, int pageSize = 10)
        {
            var query = _blogPostRepository.Table.Where(p => !p.Deleted && p.ShowOnHomePage)
                .OrderByDescending(p => p.CreatedDate);

            return query.ToPagedList(pageIndex, pageSize);
        }

        public IPagedList<BlogPost> GetPostsByCategoryId(int categoryId, int pageIndex = 1, int pageSize = 10)
        {
            var category = _blogCategoryService.GetCategoryById(categoryId);

            var allCategoryIds = new List<int> {category.Id};
            allCategoryIds.AddRange(_blogCategoryService.GetChildCategoryIds(categoryId));

            var posts = GetAllBlogPostsByBlogCategoryIds(allCategoryIds.ToArray(), pageIndex, pageSize);

            return posts;
        }

        public void InsertBlogPost(BlogPost blogPost)
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));

            blogPost.CreatedDate = DateTime.Now;
            _blogPostRepository.Insert(blogPost);
        }

        public void UpdateBlogPost(BlogPost blogPost)
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));

            _blogPostRepository.Update(blogPost);
        }

        public void DeleteBlogPost(BlogPost blogPost)
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));

            blogPost.Deleted = true;
            _blogPostRepository.Update(blogPost);

            if(!string.IsNullOrWhiteSpace(blogPost.ThumbnailImage))
                _pictureService.DeletePicture(blogPost.ThumbnailImage);
        }

        public IPagedList<BlogPost> GetBlogPostsByTerms(string terms, int pageIndex = 1, int pageSize = 10)
        {
            var query = _blogPostRepository.Table.
                Where(p => p.Name.Contains(terms) || p.ShortDescription.Contains(terms));

            query = query.OrderByDescending(p => p.CreatedDate);

            return query.ToPagedList(pageIndex, pageSize);
        }

        #endregion
    }
}
