﻿using GoCoCMS.Web.Infrastructure.Model;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoCoCMS.Web.Areas.Admin.Models.Post
{
    public class BlogPostModel : BaseEntityModel
    {
        #region Ctor

        public BlogPostModel()
        {
            this.AvailableCategories = new List<SelectListItem>();
            this.ShowOnHomePage = true;
            this.AllowComment = true;
        }

        #endregion

        #region Properties

        [Required]
        public string Name { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [Display(Name = "Short Description")]
        [MaxLength(200)]
        public string ShortDescription { get; set; }

        public string Category { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedDateString => CreatedDate.ToString("dd-MM-yyyy HH:mm");

        [Display(Name = "Show on Home page")]
        public bool ShowOnHomePage { get; set; }

        [Display(Name = "Allow Comment")]
        public bool AllowComment { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Thumbnail Image")]
        public string ThumbnailImage { get; set; }

        [Display(Name = "Category")]
        public int BlogCategoryId { get; set; }
        public IList<SelectListItem> AvailableCategories { get; set; }

        #endregion
    }
}
