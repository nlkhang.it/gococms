﻿using GoCoCMS.Web.Infrastructure.Model;
using System.Collections.Generic;

namespace GoCoCMS.Web.Areas.Admin.Models.Post
{
    public class BlogPostListModel
    {
        public BlogPostListModel()
        {
            BlogPosts = new List<BlogPostModel>();
            BlogPostSearchModel = new BlogPostSearchModel();
            PagingModelFilter = new PagingModelFilter();
        }

        public BlogPostSearchModel BlogPostSearchModel { get; set; }
        public IEnumerable<BlogPostModel> BlogPosts { get; set; }
        public PagingModelFilter PagingModelFilter { get; set; }
    }
}
