﻿using GoCoCMS.Web.Infrastructure.Model;
using System.Collections.Generic;

namespace GoCoCMS.Web.Areas.Admin.Models.Category
{
    public class CategoryListModel
    {
        public CategoryListModel()
        {
            Categories = new List<CategoryModel>();
            CategorySearchModel = new CategorySearchModel();
            PagingModelFilter = new PagingModelFilter();
        }

        public CategorySearchModel CategorySearchModel { get; set; }
        public IEnumerable<CategoryModel> Categories { get; set; }
        public PagingModelFilter PagingModelFilter { get; set; }
    }
}
