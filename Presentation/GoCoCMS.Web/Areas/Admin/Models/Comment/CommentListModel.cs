﻿using GoCoCMS.Web.Infrastructure.Model;
using System.Collections.Generic;

namespace GoCoCMS.Web.Areas.Admin.Models.Comment
{
    public class CommentListModel
    {
        public CommentListModel()
        {
            Comments = new List<CommentModel>();
            CommentSearchModel = new CommentSearchModel();
            PagingModelFilter = new PagingModelFilter();
        }

        public CommentSearchModel CommentSearchModel { get; set; }
        public IEnumerable<CommentModel> Comments { get; set; }
        public PagingModelFilter PagingModelFilter { get; set; }
    }
}
