﻿using System.ComponentModel.DataAnnotations;

namespace GoCoCMS.Web.Areas.Admin.Models.Comment
{
    public class CommentSearchModel
    {
        [Display(Name = "Comment")]
        public string Comment { get; set; }
    }
}
