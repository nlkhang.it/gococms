﻿using GoCoCMS.Web.Infrastructure.Model;
using System;

namespace GoCoCMS.Web.Areas.Admin.Models.Comment
{
    public class CommentModel : BaseEntityModel
    {
        #region Properties

        public string Content { get; set; }

        public string BlogPostName { get; set; }

        public string UserName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedDateString => CreatedDate.ToString("dd-MM-yyyy HH:mm");

        #endregion
    }
}
