﻿using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Areas.Admin.Components
{
    public class NavbarViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
