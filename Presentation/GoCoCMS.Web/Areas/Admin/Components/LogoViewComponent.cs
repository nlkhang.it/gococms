﻿using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Areas.Admin.Components
{
    public class LogoViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
