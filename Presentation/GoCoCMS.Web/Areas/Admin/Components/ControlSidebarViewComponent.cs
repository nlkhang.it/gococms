﻿using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Areas.Admin.Components
{
    public class ControlSidebarViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
