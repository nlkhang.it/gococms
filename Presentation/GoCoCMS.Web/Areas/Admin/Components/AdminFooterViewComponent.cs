﻿using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Areas.Admin.Components
{
    public class AdminFooterViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
