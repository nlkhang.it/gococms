﻿using GoCoCMS.Core.DependencyRegistrator;
using GoCoCMS.Core.Helper.FileProviders;
using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Factories;
using Microsoft.Extensions.DependencyInjection;

namespace GoCoCMS.Web.Areas.Admin.Infrastructure.DependencyRegistrator
{
    public class DependencyRegistrator : IDependencyRegistrator
    {
        public void Register(IServiceCollection serviceCollection)
        {
            // model factory
            serviceCollection.AddScoped<IBaseModelFactory, BaseModelFactory>();
            serviceCollection.AddScoped<IBlogCategoryModelFactory, BlogCategoryModelFactory>();
            serviceCollection.AddScoped<IBlogPostModelFactory, BlogPostModelFactory>();
            serviceCollection.AddScoped<IBlogCommentModelFactory, BlogCommentModelFactory>();

            // service
            serviceCollection.AddScoped<IFileProvider, FileProvider>();
            serviceCollection.AddScoped<IPictureService, PictureService>();
            serviceCollection.AddScoped<IUrlSlugService, UrlSlugService>();
        }
    }
}
