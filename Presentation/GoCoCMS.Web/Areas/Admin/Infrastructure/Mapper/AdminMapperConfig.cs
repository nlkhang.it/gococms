﻿using AutoMapper;
using GoCoCMS.Core.Mapper;
using GoCoCMS.Data.Domain;

namespace GoCoCMS.Web.Areas.Admin.Infrastructure.Mapper
{
    public class AdminMapperConfig : Profile, IMapperProfile
    {
        #region Ctor

        public AdminMapperConfig()
        {
            CreateCategoryMaps();
        }

        #endregion

        #region Methods

        protected void CreateCategoryMaps()
        {
            // category
            CreateMap<Models.Category.CategoryModel, BlogCategory>();
            CreateMap<BlogCategory, Models.Category.CategoryModel>();

            // blog post
            CreateMap<Models.Post.BlogPostModel, BlogPost>()
                .ForMember(model => model.CreatedDate, option => option.Ignore());

            CreateMap<BlogPost, Models.Post.BlogPostModel>();
        }

        #endregion
    }
}
