﻿using GoCoCMS.Web.Areas.Admin.Models.Comment;

namespace GoCoCMS.Web.Areas.Admin.Factories
{
    public interface IBlogCommentModelFactory
    {
        CommentListModel PrepareCommentListModel(CommentSearchModel commentSearchModel, int pageIndex = 1, int pageSize = 10);
    }
}
