﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Web.Areas.Admin.Models.Category;

namespace GoCoCMS.Web.Areas.Admin.Factories
{
    public interface IBlogCategoryModelFactory
    {
        CategoryListModel PrepareCategoryListModel(CategorySearchModel categorySearchModel, int pageIndex = 1, int pageSize = 10);
        CategoryModel PrepareCategoryModel(CategoryModel categoryModel, BlogCategory category);
    }
}
