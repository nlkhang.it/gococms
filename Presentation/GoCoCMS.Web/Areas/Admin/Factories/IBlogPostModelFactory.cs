﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Web.Areas.Admin.Models.Post;

namespace GoCoCMS.Web.Areas.Admin.Factories
{
    public interface IBlogPostModelFactory
    {
        BlogPostListModel PrepareBlogPostListModel(BlogPostSearchModel blogPostSearchModel, int pageIndex = 1, int pageSize = 10);
        BlogPostModel PrepareBlogPostModel(BlogPostModel blogPostModel, BlogPost blogPost);
    }
}
