﻿using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Models.Comment;
using System;
using System.Linq;
using CommentModel = GoCoCMS.Web.Areas.Admin.Models.Comment.CommentModel;

namespace GoCoCMS.Web.Areas.Admin.Factories
{
    public class BlogCommentModelFactory : IBlogCommentModelFactory
    {
        #region Fields

        private readonly ICommentService _commentService;

        #endregion

        #region Ctor

        public BlogCommentModelFactory(ICommentService commentService)
        {
            _commentService = commentService;
        }

        #endregion

        public CommentListModel PrepareCommentListModel(CommentSearchModel commentSearchModel, int pageIndex = 1, int pageSize = 10)
        {
            if (commentSearchModel == null)
                throw new ArgumentNullException(nameof(commentSearchModel));

            // get comments
            var comments = _commentService.GetAllComments(commentSearchModel.Comment, pageIndex, pageSize);

            // prepare view model
            var model = new CommentListModel()
            {
                Comments = comments.Data.Select(comment => new CommentModel()
                {
                    Id = comment.Id,
                    Content = comment.Content,
                    BlogPostName = comment.BlogPost?.Name,
                    UserName = comment.BlogPost?.User?.UserName,
                    CreatedDate = comment.CreatedDate
                })
            };
            model.PagingModelFilter.LoadPagedList(comments);

            return model;
        }
    }
}
