﻿using GoCoCMS.Core.Helper.FileProviders;
using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Domain.Identity;
using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Factories;
using GoCoCMS.Web.Areas.Admin.Models.Post;
using GoCoCMS.Web.Infrastructure.Mapper.Extensions;
using GoCoCMS.Web.Infrastructure.Mvc.ActionFillter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GoCoCMS.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class BlogPostController : Controller
    {
        #region Fields

        private readonly IBlogPostModelFactory _blogPostModelFactory;
        private readonly IBlogPostService _blogPostService;
        private readonly IFileProvider _fileProvider;
        private readonly IPictureService _pictureService;
        private readonly IUrlSlugService _urlSlugService;
        private readonly UserManager<User> _userManager;

        #endregion

        #region Ctor

        public BlogPostController(IBlogPostModelFactory blogPostModelFactory,
            IBlogPostService blogPostService,
            IFileProvider fileProvider,
            IPictureService pictureService,
            IUrlSlugService urlSlugService,
            UserManager<User> userManager)
        {
            _blogPostModelFactory = blogPostModelFactory;
            _blogPostService = blogPostService;
            _fileProvider = fileProvider;
            _pictureService = pictureService;
            _urlSlugService = urlSlugService;
            _userManager = userManager;
        }

        #endregion

        #region Methods

        // GET: BlogPost
        public ActionResult Index(BlogPostSearchModel searchModel, int page = 1)
        {
            var model = _blogPostModelFactory.PrepareBlogPostListModel(searchModel, page);

            return View(model);
        }

        // GET: BlogPost/Create
        public ActionResult Create()
        {
            var model = _blogPostModelFactory.PrepareBlogPostModel(new BlogPostModel(), null);

            return View(model);
        }

        // POST: BlogPost/Create
        [HttpPost]
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(BlogPostModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                HandleUploadFile(model);

                var blogPost = model.ToEntity<BlogPost>();
                blogPost.UserIdCreated = long.Parse(_userManager.GetUserId(User));
                _blogPostService.InsertBlogPost(blogPost);

                var seName = _urlSlugService.ValidateSeName(blogPost, string.Empty, model.Name, true);
                _urlSlugService.SaveSlug(blogPost, seName);

                if (!continueEditing)
                    return RedirectToAction("Index");

                return RedirectToAction("Create", new { id = blogPost.Id });
            }

            model = _blogPostModelFactory.PrepareBlogPostModel(model, null);

            return View(model);
        }

        // GET: BlogPost/Edit/5
        public ActionResult Edit(int id)
        {
            // try get blog post by parameter
            var blogPost = _blogPostService.GetBlogPostById(id);
            if (blogPost?.Deleted == true)
                return RedirectToAction("Index");

            // prepare model
            var model = _blogPostModelFactory.PrepareBlogPostModel(null, blogPost);

            return View(model);
        }

        // POST: BlogPost/Edit/5
        [HttpPost]
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(BlogPostModel model, bool continueEditing)
        {
            // try get blog post by parameter
            var blogPost = _blogPostService.GetBlogPostById(model.Id);
            if (blogPost == null || blogPost.Deleted)
                return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                if (Request?.Form?.Files.Any() == true)
                {
                    if(!string.IsNullOrEmpty(blogPost?.ThumbnailImage))
                        _pictureService.DeletePicture(blogPost.ThumbnailImage);

                    HandleUploadFile(model);
                }

                blogPost = model.ToEntity(blogPost);
                _blogPostService.UpdateBlogPost(blogPost);

                if (model.Name != blogPost.Name)
                {
                    var seName = _urlSlugService.ValidateSeName(blogPost, string.Empty, model.Name, true);
                    _urlSlugService.SaveSlug(blogPost, seName);
                }

                if (!continueEditing)
                    return RedirectToAction("Index");

                return RedirectToAction("Edit", new { id = blogPost?.Id });
            }

            // prepare model
            model = _blogPostModelFactory.PrepareBlogPostModel(model, blogPost);

            return View(model);
        }

        // GET: BlogPost/Delete/5
        public ActionResult Delete(int id)
        {
            // try get blog post by parameter id
            var blogPost = _blogPostService.GetBlogPostById(id);
            if (blogPost == null || blogPost.Deleted)
                return RedirectToAction("Index");

            // delete blog post
            _blogPostService.DeleteBlogPost(blogPost);

            return RedirectToAction("Index");
        }

        #endregion

        #region Utilities

        protected void HandleUploadFile(BlogPostModel model)
        {
            // handle upload file
            var httpPostedFile = Request?.Form?.Files?.FirstOrDefault();
            if (httpPostedFile == null) return;

            var fileBinary = _fileProvider.GetBytesFromFile(httpPostedFile);
            var fileName = httpPostedFile.FileName;
            var contentType = httpPostedFile.ContentType;

            var fileExtension = _fileProvider.GetFileExtension(fileName);
            if (!string.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            //contentType is not always available 
            //that's why we manually update it here
            //http://www.sfsu.edu/training/mimetype.htm
            if (string.IsNullOrEmpty(contentType))
            {
                switch (fileExtension)
                {
                    case ".bmp":
                        contentType = MimeTypes.ImageBmp;
                        break;
                    case ".gif":
                        contentType = MimeTypes.ImageGif;
                        break;
                    case ".jpeg":
                    case ".jpg":
                    case ".jpe":
                    case ".jfif":
                    case ".pjpeg":
                    case ".pjp":
                        contentType = MimeTypes.ImageJpeg;
                        break;
                    case ".png":
                        contentType = MimeTypes.ImagePng;
                        break;
                    case ".tiff":
                    case ".tif":
                        contentType = MimeTypes.ImageTiff;
                        break;
                    default:
                        break;
                }
            }

            var outputFileName = string.Empty;
            _pictureService.SavePictureInFile(fileBinary, contentType, ref outputFileName);
            model.ThumbnailImage = outputFileName;
        }

        #endregion
    }
}