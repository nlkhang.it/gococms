﻿using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Factories;
using GoCoCMS.Web.Areas.Admin.Models.Comment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class CommentController : Controller
    {
        #region Fields

        private readonly IBlogCommentModelFactory _blogCommentModelFactory;
        private readonly ICommentService _commentService;

        #endregion

        #region Ctor

        public CommentController(IBlogCommentModelFactory blogCommentModelFactory,
            ICommentService commentService)
        {
            _blogCommentModelFactory = blogCommentModelFactory;
            _commentService = commentService;
        }

        #endregion

        #region Methods

        public ActionResult Index(CommentSearchModel searchModel, int page = 1)
        {
            var model = _blogCommentModelFactory.PrepareCommentListModel(searchModel, page);

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            // try get blog post by parameter id
            var comment = _commentService.GetCommentById(id);
            if (comment == null || comment.Deleted)
                return RedirectToAction("Index");

            // delete blog post
            _commentService.DeleteComment(comment);

            return RedirectToAction("Index");
        }

        #endregion
    }
}
