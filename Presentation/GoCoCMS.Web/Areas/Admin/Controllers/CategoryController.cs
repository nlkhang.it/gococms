﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Service;
using GoCoCMS.Web.Areas.Admin.Factories;
using GoCoCMS.Web.Areas.Admin.Models.Category;
using GoCoCMS.Web.Infrastructure.Mapper.Extensions;
using GoCoCMS.Web.Infrastructure.Mvc.ActionFillter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class CategoryController : Controller
    {
        #region Fields

        private readonly IBlogCategoryModelFactory _categoryModelFactory;
        private readonly IBlogCategoryService _categoryService;
        private readonly IUrlSlugService _urlSlugService;

        #endregion

        #region Ctor

        public CategoryController(IBlogCategoryModelFactory categoryModelFactory,
            IBlogCategoryService categoryService,
            IUrlSlugService urlSlugService)
        {
            _categoryModelFactory = categoryModelFactory;
            _categoryService = categoryService;
            _urlSlugService = urlSlugService;
        }

        #endregion

        #region Methods

        // GET: Category
        public ActionResult Index(CategorySearchModel searchModel, int page = 1)
        {
            var model = _categoryModelFactory.PrepareCategoryListModel(searchModel, page);

            return View(model);
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            var model = _categoryModelFactory.PrepareCategoryModel(new CategoryModel(), null);

            return View(model);
        }

        // POST: Category/Create
        [HttpPost]
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(CategoryModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var category = model.ToEntity<BlogCategory>();
                _categoryService.InsertCategory(category);

                var slug = _urlSlugService.ValidateSeName(category, string.Empty, model.Name, true);
                _urlSlugService.SaveSlug(category, slug);

                if (!continueEditing)
                    return RedirectToAction("Index");

                return RedirectToAction("Create", new { id = category.Id });
            }

            model = _categoryModelFactory.PrepareCategoryModel(model, null);

            return View(model);
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            // try get category by parameter
            var category = _categoryService.GetCategoryById(id);
            if (category == null || category.Deleted)
                return RedirectToAction("Index");

            // prepare model
            var model = _categoryModelFactory.PrepareCategoryModel(null, category);

            return View(model);
        }

        // POST: Category/Edit/5
        [HttpPost]
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]    
        public ActionResult Edit(CategoryModel model, bool continueEditing)
        {
            // try get category by parameter
            var category = _categoryService.GetCategoryById(model.Id);
            if (category == null || category.Deleted)
                return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                category = model.ToEntity(category);
                _categoryService.UpdateCategory(category);

                if (model.Name != category.Name)
                {
                    var slug = _urlSlugService.ValidateSeName(category, string.Empty, model.Name, true);
                    _urlSlugService.SaveSlug(category, slug);
                }

                if (!continueEditing)
                    return RedirectToAction("Index");

                return RedirectToAction("Edit", new { id = category.Id });
            }

            // prepare model
            model = _categoryModelFactory.PrepareCategoryModel(model, category);

            return View(model);
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            // try get category by parameter id
            var category = _categoryService.GetCategoryById(id);
            if(category?.Deleted == true)
                return RedirectToAction("Index");

            // delete category
            _categoryService.DeleteCategory(category);

            return RedirectToAction("Index");
        }

        #endregion
    }
}