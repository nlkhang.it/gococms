﻿using AutoMapper;
using GoCoCMS.Core.Mapper;
using GoCoCMS.Data.Domain;
using GoCoCMS.Web.Models.Category;
using GoCoCMS.Web.Models.Post;

namespace GoCoCMS.Web.Infrastructure.Mapper
{
    public class MapperConfig : Profile, IMapperProfile
    {
        #region Ctor

        public MapperConfig()
        {
            CreateCategoryMaps();
        }

        #endregion

        #region Methods

        protected void CreateCategoryMaps()
        {
            // category
            CreateMap<CategoryModel, BlogCategory>();
            CreateMap<BlogCategory, CategoryModel>()
                .ForMember(model => model.Slug, option => option.Ignore());

            CreateMap<PostModel, BlogPost>();
            CreateMap<BlogPost, PostModel>()
                .ForMember(model => model.Slug, option => option.Ignore())
                .ForMember(model => model.User, option => option.MapFrom(p => p.User));
        }

        #endregion
    }
}
