﻿namespace GoCoCMS.Web.Models.Comment
{
    public class CommentModel
    {
        public string Content { get; set; }
        public int BlogPostId { get; set; }
        public string UserName { get; set; }
    }
}
