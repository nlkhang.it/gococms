﻿using GoCoCMS.Web.Infrastructure.Model;
using System.Collections.Generic;

namespace GoCoCMS.Web.Models.Post
{
    public class BlogPostListModel
    {
        public BlogPostListModel()
        {
            BlogPosts = new List<PostModel>();
            PagingModelFilter = new PagingModelFilter();
        }

        public IEnumerable<PostModel> BlogPosts { get; set; }
        public PagingModelFilter PagingModelFilter { get; set; }
    }
}
