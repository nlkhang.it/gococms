﻿using GoCoCMS.Web.Infrastructure.Model;
using GoCoCMS.Web.Models.User;
using System;

namespace GoCoCMS.Web.Models.Post
{
    public class PostModel : BaseEntityModel
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string Slug { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool AllowComment { get; set; }
        public string CreatedDateString => CreatedDate.ToString("dddd, dd MMMM yyyy");
        public UserModel User { get; set; }
    }
}
