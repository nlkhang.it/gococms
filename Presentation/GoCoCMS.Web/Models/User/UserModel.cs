﻿namespace GoCoCMS.Web.Models.User
{
    public class UserModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
