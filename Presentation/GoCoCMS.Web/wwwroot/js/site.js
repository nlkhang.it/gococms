﻿// Icon remove input text
$('.has-clear input[type="text"]').on('input propertychange', function () {
    var $this = $(this);
    var visible = Boolean($this.val());
    $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
}).trigger('propertychange');

$('.form-control-clear').click(function () {
    $(this).siblings('input[type="text"]').val('')
        .trigger('propertychange').focus();
});

$('.date').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
    }
});

// ckeditor
$(".ckeditor").ckeditor();

// Upload Image
$(".UploadImage").change(function () {
    var file = this.files;
    if (file && file[0]) {
        ReadImage(file[0]);
    }
});

var ReadImage = function (file) {
    var reader = new FileReader;
    var image = new Image;
    reader.readAsDataURL(file);
    reader.onload = function (targetFile) {
        image.src = targetFile.target.result;
        image.onload = function () {
            $("#targetImg").attr("src", targetFile.target.result);
            $("#imgPreview").show();
        }
    }
}

var ClearPreview = function () {
    $(".UploadImage").val("");
    $("#imgPreview").hide();
}
