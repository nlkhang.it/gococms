﻿var GoCoCMS = {};
GoCoCMS.Comment = (function() {
    return {
        CommentPost: function (data, callback) {
            $.ajax({
                url: "/Comment/CommentPost",
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRF-TOKEN",
                        $('input:hidden[name="__RequestVerificationToken"]').val());
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (result) {
                    callback(result);
                },
                error: function (xhr, status, error) {
                    // handle error
                }
            });
        }
    };
}());

$(document).ready(function () {
    $("#comment-post").click(function () {
        // get data input
        var postId = $("#BlogPostId").val();
        var content = $("#Content").val();
        if (!content)
            return;

        // call api
        var data = { Content: content, BlogPostId: postId };
        GoCoCMS.Comment.CommentPost(data, function (result) {
            $("#comment-list").prepend(result);
            $("#Content").val("");
        }); 
    });
});