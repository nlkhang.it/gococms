﻿using GoCoCMS.Web.Factories;
using GoCoCMS.Web.Models.Common;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace GoCoCMS.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Fields

        private readonly IPostModelFactory _postModelFactory;

        #endregion

        #region Ctor

        public HomeController(IPostModelFactory postModelFactory)
        {
            _postModelFactory = postModelFactory;
        }

        #endregion

        #region MethodsPrepareHomePagePostListModel

        public IActionResult Index(int page = 1, int pageSize = 5)
        {
            var postListModel = _postModelFactory.PrepareHomePagePostListModel(page, pageSize);

            return View(postListModel);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion
    }
}
