﻿using GoCoCMS.Web.Factories;
using GoCoCMS.Web.Models.Post;
using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Controllers
{
    public class PostController : Controller
    {
        #region Fields

        private readonly IPostModelFactory _postModelFactory;

        #endregion

        #region Ctor

        public PostController(IPostModelFactory postModelFactory)
        {
            _postModelFactory = postModelFactory;
        }

        #endregion

        #region Methods

        public IActionResult PostsByCategory(int categoryId, int page = 1, int pageSize = 10)
        {
            var postListModel = _postModelFactory.PreparePostsByCategoryModel(categoryId, page, pageSize);

            return View(postListModel);
        }

        public IActionResult Details(int id)
        {
            var postModel = _postModelFactory.PreparePostModelById(id);
            return View(postModel);
        }

        public IActionResult Search(SearchModel model, int page = 1, int pageSize = 10)
        {
            var postListModel = _postModelFactory.PrepareSearchModel(model.Term, page, pageSize);

            return View(postListModel);
        }

        #endregion
    }
}