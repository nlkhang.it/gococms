﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Domain.Identity;
using GoCoCMS.Service;
using GoCoCMS.Web.Models.Comment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;

namespace GoCoCMS.Web.Controllers
{
    [Authorize]
    public class CommentController : BaseController
    {
        #region Fields

        private readonly ICommentService _commentService;
        private readonly UserManager<User> _userManager;

        #endregion

        #region Ctor

        public CommentController(ICommentService commentService,
            UserManager<User> userManager,
            ICompositeViewEngine viewEngine) : base(viewEngine)
        {
            _commentService = commentService;
            _userManager = userManager;
        }

            #endregion

        [HttpPost]
        public IActionResult CommentPost([FromBody]CommentModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Content) || model.BlogPostId == 0)
                return Json(null);

            // get user id
            var userString = _userManager.GetUserId(User);
            if(!long.TryParse(userString, out _))
                return Json(null);            

            var comment = new Comment
            {
                Content = model.Content,
                BlogPostId = model.BlogPostId
            };
            _commentService.InsertComment(comment);

            model.UserName = _userManager.GetUserName(User);
            var modelString = RenderViewAsString(model, "_CommentBox");

            return Json(modelString);
        }
    }
}