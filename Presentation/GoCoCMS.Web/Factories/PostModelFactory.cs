﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Service;
using GoCoCMS.Web.Infrastructure.Mapper.Extensions;
using GoCoCMS.Web.Models.Post;
using GoCoCMS.Web.Models.User;
using System.Collections.Generic;
using System.Linq;

namespace GoCoCMS.Web.Factories
{
    public class PostModelFactory : IPostModelFactory
    {
        #region Fields

        private readonly IBlogPostService _blogPostService;
        private readonly IUrlSlugService _urlSlugService;

        #endregion

        #region Ctor

        public PostModelFactory(IBlogPostService blogPostService,
            IUrlSlugService urlSlugService)
        {
            _blogPostService = blogPostService;
            _urlSlugService = urlSlugService;
        }

        #endregion

        #region Methods

        public IList<PostModel> PrepareRecentPostModel()
        {
            var posts =  _blogPostService.GetRecentPosts(10);
            var postModels = PostModels(posts);
            return postModels;
        }

        public BlogPostListModel PrepareHomePagePostListModel(int pageIndex = 1, int pageSize = 10)
        {
            var posts = _blogPostService.GetPostsOnHomePage(pageIndex, pageSize);
            var postModels = PostModels(posts.Data);

            var model = new BlogPostListModel { BlogPosts = postModels };
            model.PagingModelFilter.LoadPagedList(posts);

            return model;
        }

        public BlogPostListModel PreparePostsByCategoryModel(int categoryId, int pageIndex = 1, int pageSize = 10)
        {
            var posts = _blogPostService.GetPostsByCategoryId(categoryId, pageIndex, pageSize);
            var postModels = PostModels(posts.Data);
            var model = new BlogPostListModel { BlogPosts = postModels };
            model.PagingModelFilter.LoadPagedList(posts);

            return model;
        }

        public PostModel PreparePostModelById(int id)
        {
            var post = _blogPostService.GetBlogPostById(id);
            var slug = _urlSlugService.GetSlug(post);
            var postModel = post.ToModel<PostModel>();
            postModel.Slug = slug;

            return postModel;
        }

        public BlogPostListModel PrepareSearchModel(string terms, int pageIndex = 1, int pageSize = 10)
        {
            var posts = _blogPostService.GetBlogPostsByTerms(terms, pageIndex, pageSize);
            var postModels = PostModels(posts.Data);
            var model = new BlogPostListModel { BlogPosts = postModels };
            model.PagingModelFilter.LoadPagedList(posts);

            return model;
        }

        #endregion

        #region Utilities

        private List<PostModel> PostModels(IEnumerable<BlogPost> posts)
        {
            var postModels = posts.Select(p => new PostModel
            {
                Id = p.Id,
                Name = p.Name,
                Content = p.Content,
                CreatedDate = p.CreatedDate,
                ShortDescription = p.ShortDescription,
                ThumbnailImage = p.ThumbnailImage,
                AllowComment = p.AllowComment,
                Slug = _urlSlugService.GetSlug(p.Id, nameof(BlogPost)),
                User = new UserModel
                {
                    UserName = p.User.UserName,
                    Email = p.User.Email
                }
            }).ToList();
            return postModels;
        }

        #endregion
    }
}
