﻿using GoCoCMS.Web.Models.Post;
using System.Collections.Generic;

namespace GoCoCMS.Web.Factories
{
    public interface IPostModelFactory
    {
        IList<PostModel> PrepareRecentPostModel();
        BlogPostListModel PrepareHomePagePostListModel(int pageIndex = 1, int pageSize = 10);
        BlogPostListModel PreparePostsByCategoryModel(int categoryId, int pageIndex = 1, int pageSize = 10);
        PostModel PreparePostModelById(int id);
        BlogPostListModel PrepareSearchModel(string terms, int page = 1, int pageSize = 10);
    }
}
