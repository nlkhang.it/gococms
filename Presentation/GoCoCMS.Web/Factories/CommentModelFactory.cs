﻿using GoCoCMS.Service;
using GoCoCMS.Web.Models.Comment;
using System.Collections.Generic;
using System.Linq;

namespace GoCoCMS.Web.Factories
{
    public class CommentModelFactory : ICommentModelFactory
    {
        #region Fields

        private readonly ICommentService _commentService;

        #endregion

        #region Ctors

        public CommentModelFactory(ICommentService commentService)
        {
            _commentService = commentService;
        }

        #endregion

        #region Methods

        public IList<CommentModel> PrepareCommentModelByPostId(int postId)
        {
            var comments = _commentService.GetCommentByPostId(postId);
            var commentModels = comments.Select(c => new CommentModel
            {
                Content = c.Content,
                BlogPostId = c.BlogPostId,
                UserName = c.BlogPost?.User?.UserName
            });

            return commentModels.ToList();
        }

        #endregion
    }
}
