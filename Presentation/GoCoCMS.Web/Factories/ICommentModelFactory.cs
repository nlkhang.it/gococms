﻿using GoCoCMS.Web.Models.Comment;
using System.Collections.Generic;

namespace GoCoCMS.Web.Factories
{
    public interface ICommentModelFactory
    {
        IList<CommentModel> PrepareCommentModelByPostId(int postId);
    }
}
