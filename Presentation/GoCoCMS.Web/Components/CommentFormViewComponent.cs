﻿using GoCoCMS.Web.Models.Comment;
using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Components
{
    public class CommentFormViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(int postId)
        {
            return View(new CommentModel{ BlogPostId = postId});
        }
    }
}
