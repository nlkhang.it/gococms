﻿using GoCoCMS.Web.Factories;
using Microsoft.AspNetCore.Mvc;

namespace GoCoCMS.Web.Components
{
    public class CommentListViewComponent : ViewComponent
    {
        private readonly ICommentModelFactory _commentModelFactory;

        public CommentListViewComponent(ICommentModelFactory commentModelFactory)
        {
            _commentModelFactory = commentModelFactory;
        }

        public IViewComponentResult Invoke(int postId)
        {
            var commentModels = _commentModelFactory.PrepareCommentModelByPostId(postId);

            return View(commentModels);
        }
    }
}
