﻿using GoCoCMS.Web.Infrastructure.Routing;
using Microsoft.AspNetCore.Builder;

namespace GoCoCMS.Web.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtension
    {
        public static void MapRoute(this IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.Routes.Add(new SlugRouter(routes.DefaultHandler));

                routes.MapRoute(
                    "areaRoute",
                    "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute("Login", "login/",
                    new { controller = "Account", action = "Login" });

                routes.MapRoute("Register", "register/",
                    new { controller = "Account", action = "Register" });

                routes.MapRoute("Logout", "logout/",
                    new { controller = "Account", action = "Lockout" });

                routes.MapRoute("About", "about/",
                    new { controller = "Home", action = "About" });

                routes.MapRoute("Contact", "contact/",
                    new { controller = "Home", action = "Contact" });

                routes.MapRoute("Admin", "admin/",
                    new { Areas = "Admin", controller = "Home", action = "Index" });
            });
        }
    }
}
