﻿using AutoMapper;
using GoCoCMS.Core.Caching;
using GoCoCMS.Core.DependencyRegistrator;
using GoCoCMS.Core.Mapper;
using GoCoCMS.Data;
using GoCoCMS.Data.Domain.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace GoCoCMS.Web.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void ConfigureApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMemoryCache();
            services.AddMvc(options =>
                {
                    // Enforce HTTPS
                    options.Filters.Add(new RequireHttpsAttribute());

                    // Anti-request forgery to global app
                    options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAntiforgery(options => options.HeaderName = "X-CSRF-TOKEN");

            // google 
            var clientIdConfig = configuration.GetSection("ExternalAuthentication:Google:ClientId");
            var clientSecretConfig = configuration.GetSection("ExternalAuthentication:Google:ClientSecret");
            services.AddAuthentication().AddGoogle(options =>
            {
                options.ClientId = clientIdConfig.Value;
                options.ClientSecret = clientSecretConfig.Value;
            });

            // dependency for db context
            services.AddDbContext<GoCoCmsContext>(
                options => options.UseSqlServer(configuration.GetConnectionString("SqlConnection")));

            // Identity
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<GoCoCmsContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options => options.LoginPath = "/Account/Login");

            // caching
            services.AddScoped<ICacheManager, PerRequestCacheManager>();

            var redisCacheConfiguration = configuration.GetSection("RedisCacheConfiguration:EnableRedisCached");
            if (bool.TryParse(redisCacheConfiguration.Value, out var enableRedisCached) && enableRedisCached)
            {
                services.AddSingleton<IRedisConnectionWrapper, RedisConnectionWrapper>();
                services.AddScoped<IStaticCacheManager, RedisCacheManager>();
            }
            else
            {
                services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
            }
        }

        public static void AddDependencyInjection(this IServiceCollection services)
        {
            // find dependency registrator
            var dependencyRegistratorType = typeof(IDependencyRegistrator);
            var dependencyRegistratorImplementTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(t => t.GetTypes())
                .Where(t => dependencyRegistratorType.IsAssignableFrom(t) && t.IsClass && !t.IsAbstract);

            // get instance
            var instances = dependencyRegistratorImplementTypes.Select(dependencyRegistrator =>
                (IDependencyRegistrator)Activator.CreateInstance(dependencyRegistrator));

            // register instance of type
            foreach (var instance in instances)
                instance.Register(services);
        }

        public static void AddAutoMapper(this IServiceCollection services)
        {
            // get all instances of IMapperProfile
            var mapperProfileType = typeof(IMapperProfile);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(t => t.GetTypes())
                .Where(t => mapperProfileType.IsAssignableFrom(t) && t.IsClass && !t.IsAbstract);

            // mapping config
            var mappingConfig = new MapperConfiguration(mc =>
            {
                foreach (var type in types)
                    mc.AddProfile(type);
            });

            // init auto mapper
            AutoMapperConfiguration.Init(mappingConfig);
        }
    }
}
