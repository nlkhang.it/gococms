﻿using GoCoCMS.Core.Paging;

namespace GoCoCMS.Web.Infrastructure.Model
{
    public class BasePagingModel
    {
        public virtual void LoadPagedList<T>(IPagedList<T> pagedList)
        {
            PageIndex = pagedList.PageIndex;
            PageSize = pagedList.PageSize;
            TotalItems = pagedList.TotalItems;
            TotalPages = pagedList.TotalPages;
            HasPreviousPage = pagedList.HasPreviousPage;
            HasNextPage = pagedList.HasNextPage;
        }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
    }
}
