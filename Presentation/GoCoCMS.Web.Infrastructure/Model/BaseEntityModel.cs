﻿namespace GoCoCMS.Web.Infrastructure.Model
{
    public class BaseEntityModel
    {
        public int Id { get; set; }
    }
}
