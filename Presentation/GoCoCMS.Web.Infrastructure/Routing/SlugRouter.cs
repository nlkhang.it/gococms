﻿using GoCoCMS.Data.Domain;
using GoCoCMS.Data.Repositories;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Threading.Tasks;

namespace GoCoCMS.Web.Infrastructure.Routing
{
    public class SlugRouter : IRouter
    {
        private readonly IRouter _target;

        public SlugRouter(IRouter target)
        {
            _target = target;
        }

        public async Task RouteAsync(RouteContext context)
        {
            var requestPath = context.HttpContext.Request.Path.Value;

            if (!string.IsNullOrEmpty(requestPath) && requestPath[0] == '/')
            {
                // Trim the leading slash
                requestPath = requestPath.Substring(1);
            }

            var urlSlugRepository = context.HttpContext.RequestServices.GetService<IRepository<UrlSlug>>();

            // Get the slug that matches.
            var urlSlug = urlSlugRepository.Table.FirstOrDefault(s => s.Slug == requestPath);

            // If we got back a null value set, that means the URI did not match)
            if (urlSlug == null) return;

            // Invoke MVC controller/action
            var oldRouteData = context.RouteData;
            var newRouteData = new RouteData(oldRouteData);
            newRouteData.Routers.Add(_target);

            switch (urlSlug.EntityName.ToLowerInvariant())
            {
                case "blogpost":
                    newRouteData.Values["controller"] = "Post";
                    newRouteData.Values["action"] = "Details";
                    newRouteData.Values["id"] = urlSlug.EntityId;
                    newRouteData.Values["SeName"] = urlSlug.Slug;
                    break;

                case "blogcategory":
                    newRouteData.Values["controller"] = "Post";
                    newRouteData.Values["action"] = "PostsByCategory";
                    newRouteData.Values["categoryId"] = urlSlug.EntityId;
                    newRouteData.Values["SeName"] = urlSlug.Slug;
                    break;

                default:
                    break;
            }

            context.RouteData = newRouteData;
            await _target.RouteAsync(context);
        }

        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            return null;
        }
    }
}
