﻿using GoCoCMS.Data.Domain.Identity;
using Microsoft.AspNetCore.Identity;

namespace GoCoCMS.Web.Infrastructure.DataInitializer
{
    public static class IdentityDataInitializer
    {
        public static void SeedData
        (UserManager<User> userManager,
            RoleManager<Role> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        public static void SeedUsers
            (UserManager<User> userManager)
        {
            if (userManager.FindByNameAsync
                    ("admin").Result == null)
            {
                var user = new User
                {
                    UserName = "admin@gococms.com",
                    Email = "admin@gococms.com"
                };

                var result = userManager.CreateAsync
                    (user, "P@ssw0rd").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user,
                        "Administrator").Wait();
                }
            }
        }

        public static void SeedRoles
            (RoleManager<Role> roleManager)
        {
            if (roleManager.RoleExistsAsync
                ("Administrator").Result) return;

            var role = new Role {Name = "Administrator"};
            var roleResult = roleManager.CreateAsync(role).Result;
        }
    }
}
