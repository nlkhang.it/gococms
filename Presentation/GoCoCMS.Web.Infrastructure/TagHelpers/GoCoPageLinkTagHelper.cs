﻿using GoCoCMS.Core.Web;
using GoCoCMS.Web.Infrastructure.Model;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using System.Linq;

namespace GoCoCMS.Web.Infrastructure.TagHelpers
{
    [HtmlTargetElement("goco-paging", Attributes = "page-model", TagStructure = TagStructure.WithoutEndTag)]
    public class GoCoPageLinkTagHelper : TagHelper
    {
        private readonly IUrlHelperFactory _urlHelperFactory;
        private readonly IWebHelper _webHelper;

        public GoCoPageLinkTagHelper(IUrlHelperFactory urlHelperFactory,
            IWebHelper webHelper)
        {
            _urlHelperFactory = urlHelperFactory;
            _webHelper = webHelper;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        public PagingModelFilter PageModel { get; set; }
        public string PageAction { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix = "page-url-")]
        public Dictionary<string, object> PageUrlValues { get; set; } = new Dictionary<string, object>();

        public string PageClass { get; set; }
        public string PageClassSelected { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if(PageModel.TotalPages < 1)
                return;

            var result = new TagBuilder("ul");

            // add previous link
            AddLiElement(result, "Previous", !PageModel.HasPreviousPage, PageModel.PageIndex - 1);

            for (var i = 1; i <= PageModel.TotalPages; i++)
            {
                // li tag
                var liTagBuilder = new TagBuilder("li");
                liTagBuilder.AddCssClass("page-item");

                // add li selected class
                if (i == PageModel.PageIndex)
                {
                    liTagBuilder.AddCssClass(PageClassSelected);
                    liTagBuilder.InnerHtml.AppendHtml($"<span class='{PageClass}'>{i}</span>");
                }
                else
                {
                    liTagBuilder.InnerHtml.AppendHtml(AddLinkTagBuilder(i.ToString(), i));
                }

                result.InnerHtml.AppendHtml(liTagBuilder);
            }

            // add next link
            AddLiElement(result, "Next", !PageModel.HasNextPage, PageModel.PageIndex + 1);

            output.TagName = "ul";
            output.TagMode = TagMode.StartTagAndEndTag;
            var classes = output.Attributes.FirstOrDefault(a => a.Name == "class")?.Value;
            output.Attributes.SetAttribute("class", $"pagination {classes}");
            output.Content.AppendHtml(result.InnerHtml);
        }

        private void AddLiElement(TagBuilder result, string content, bool isDisabled, int linkValue)
        {
            var tagBuilder = new TagBuilder("li");
            tagBuilder.AddCssClass("page-item");

            if (isDisabled)
            {
                tagBuilder.AddCssClass("disabled");
                tagBuilder.InnerHtml.AppendHtml($"<span class='{PageClass}'>{content}</span>");
            }
            else
            {
                tagBuilder.InnerHtml.AppendHtml(AddLinkTagBuilder(content, linkValue));
            }

            result.InnerHtml.AppendHtml(tagBuilder);
        }

        private TagBuilder AddLinkTagBuilder(string content, int linkValue)
        {
            var tag = new TagBuilder("a");

            tag.MergeAttribute("href", CreateHrefUrl(linkValue));

            // add a link normal class
            tag.AddCssClass(PageClass);

            tag.InnerHtml.Append(content);
            return tag;
        }

        protected virtual string CreateHrefUrl(int pageNumber)
        {
            var routeValues = new RouteValueDictionary();

            var parametersWithEmptyValues = new List<string>();
            foreach (var key in ViewContext.HttpContext.Request.Query.Keys.Where(key => key != null))
            {
                var value = ViewContext.HttpContext.Request.Query[key].ToString();
                if (string.IsNullOrEmpty(value))
                {
                    parametersWithEmptyValues.Add(key);
                }
                else
                {
                    routeValues[key] = value;
                }
            }

            routeValues["page"] = pageNumber;

            var url = _webHelper.GetThisPageUrl(false);
            foreach (var routeValue in routeValues)
            {
                url = _webHelper.ModifyQueryString(url, routeValue.Key, routeValue.Value?.ToString());
            }
            if (parametersWithEmptyValues.Any())
            {
                foreach (var key in parametersWithEmptyValues)
                {
                    url = _webHelper.ModifyQueryString(url, key);
                }
            }

            return url;
        }
    }
}
